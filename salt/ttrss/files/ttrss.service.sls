[Unit]
Description=ttrss_backend
After=network.target postgresql.service

[Service]
Type=simple
User=ttrss
WorkingDirectory=/var/www/ttrss/
ExecStart=/var/www/ttrss/update_daemon2.php --tasks {{ settings.threads }}
SyslogIdentifier=ttrss

[Install]
WantedBy=multi-user.target
