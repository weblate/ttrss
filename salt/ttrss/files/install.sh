#!/bin/bash

sleep 1

curl -s \
    --output /dev/null \
    --header 'Host: {{ settings.hostname }}' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data 'op=installschema' \
    --data 'DB_TYPE=pgsql'  \
    --data 'DB_USER=ttrss' \
    --data 'DB_PASS={{ settings.db_password }}' \
    --data 'DB_NAME=ttrss_prod' \
    --data 'DB_HOST=localhost' \
    --data 'DB_PORT=5432' \
    --data 'SELF_URL_PATH=https://{{ settings.hostname }}' \
    'https://localhost/install/' || exit 1

echo "Database initialized"

curl -s \
    --output /dev/null \
    --header 'Host: {{ settings.hostname }}' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data 'op=saveconfig' \
    --data 'DB_TYPE=pgsql'  \
    --data 'DB_USER=ttrss' \
    --data 'DB_PASS={{ settings.db_password }}' \
    --data 'DB_NAME=ttrss_prod' \
    --data 'DB_HOST=localhost' \
    --data 'DB_PORT=5432' \
    --data 'SELF_URL_PATH=https://{{ settings.hostname }}' \
    'https://localhost/install/' || exit 1

echo "Configuration saved"

sed -i /var/www/ttrss/config.php \
    -e "s/ENABLE_REGISTRATION', .*/ENABLE_REGISTRATION', {{ settings.registration.activated }});/" \
    -e "s/REG_NOTIFY_ADDRESS', '.*/REG_NOTIFY_ADDRESS', '{{ settings.admin.email }}');/" \
    -e "s/REG_MAX_USERS', .*/REG_MAX_USERS', {{ settings.registration.accounts_number }});/" \
    -e "s/SMTP_FROM_NAME', '.*/SMTP_FROM_NAME', '{{ settings.smtp.from_name }}');/" \
    -e "s/SMTP_FROM_ADDRESS', '.*/SMTP_FROM_ADDRESS', '{{ settings.smtp.from_address }}');/" \
    -e "s/DIGEST_SUBJECT', '.*/DIGEST_SUBJECT', '{{ settings.smtp.digest_subject }}');/" || exit 1

echo "Configuration modified"

curl -s \
    --cookie-jar /tmp/curl-cookiejar \
    --output /dev/null \
    --header 'Host: {{ settings.hostname }}' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data 'op=login'  \
    --data 'login=admin' \
    --data 'password=password' \
    --data 'profile=0' \
    'https://localhost/public.php' || exit 1

echo "Logged in as admin"

CSRF_TOKEN=$(
curl -s \
    --cookie-jar /tmp/curl-cookiejar \
    --cookie /tmp/curl-cookiejar \
    --header 'Host: {{ settings.hostname }}' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data 'op=rpc' \
    --data 'method=sanityCheck' \
    'https://localhost/backend.php' | jq -r '.["init-params"]'.csrf_token
)

curl -s \
    --cookie-jar /tmp/curl-cookiejar \
    --cookie /tmp/curl-cookiejar \
    --output /dev/null \
    --header 'Host: {{ settings.hostname }}' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data 'op=pref-prefs' \
    --data 'method=changepassword' \
    --data 'old_password=password' \
    --data 'new_password={{ settings.admin.password }}' \
    --data 'confirm_password={{ settings.admin.password }}' \
    --data "csrf_token=${CSRF_TOKEN}" \
    'https://localhost/backend.php' || exit 1

echo "Admin password modified"

curl -s \
    --cookie-jar /tmp/curl-cookiejar \
    --cookie /tmp/curl-cookiejar \
    --output /dev/null \
    --header 'Host: {{ settings.hostname }}' \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --data 'op=pref-prefs' \
    --data 'method=changeemail' \
    --data 'full_name=admin' \
    --data 'email={{ settings.admin.email }}' \
    --data "csrf_token=${CSRF_TOKEN}" \
    'https://localhost/backend.php' || exit 1

echo "Admin email modified"
