#!/bin/bash
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes
borg='/usr/bin/borg'
target='/var/www/ttrss_daily_backups/borg/'
src='/var/www/ttrss/'
exclude='-e /var/www/ttrss_daily_backups/borg/'

## Backup the database
## Dump of ttrss_prod database, suitable for pg_restore
pg_dump --format custom -f /var/www/ttrss_daily_backups/postgresql/ttrss_prod.$(date +%A).bak -d ttrss_prod

## Show me the backups
$borg list $target

## Backup Peertube
$borg create -v --stats $exclude $target::ttrss-{now:%Y-%m-%d_%H-%M} $src

## Check the backups
$borg check $target

## Prune old backups
$borg prune -v --list --save-space -w 4 -d 10 --prefix='ttrss' $target
